import pyautogui
import os
import re
from array import *


def main():
    checkForDirectory()
    updateBoard()


def updateBoard():
    #board_location = pyautogui.locateOnScreen('./locate images/x.png',grayscale=False,confidence = .8)
    #print(board_location)
    #point = pyautogui.center(board_location)

    water_list = []
    waves_list = []
    teal_pentagon = []
    teal_circle = []
    blue_square = []




    for pos in pyautogui.locateAllOnScreen('./locate images/water.png',confidence = .8):
        water_list.append(pos)
    for pos in pyautogui.locateAllOnScreen('./locate images/waves.png',confidence = .8):
        waves_list.append(pos)
    for pos in pyautogui.locateAllOnScreen('./locate images/teal_pentagon.png',confidence = .8):
        teal_pentagon.append(pos)
    for pos in pyautogui.locateAllOnScreen('./locate images/teal_circle.png',confidence = .8):
        teal_circle.append(pos)
    for pos in pyautogui.locateAllOnScreen('./locate images/blue_square.png',confidence = .8):
        blue_square.append(pos)

    water_list_parsed = []
    waves_list_parsed = []
    teal_circle_parsed = []
    teal_pentagon_parsed = []
    blue_square_parsed = []


    for x in water_list:
        water_list_parsed = (int, re.findall(r'\d+', str(x)))
    for x in waves_list:
        waves_list_parsed = (int, re.findall(r'\d+', str(x)))
    for x in teal_pentagon:
        teal_pentagon_parsed = (int, re.findall(r'\d+', str(x)))
    for x in teal_circle:
        teal_circle_parsed = (int, re.findall(r'\d+', str(x)))
    for x in blue_square:
        blue_square_parsed = (int, re.findall(r'\d+', str(x)))

    print(water_list_parsed[1])



#Checks for directory to save images, creates one if it doesn't exist
def checkForDirectory():
    if (os.path.isdir("./images")) == False:
        dirName = "images"
        os.mkdir(dirName)
        print("Directory ", dirName, " Created ")

main()
